#!/bin/bash
# Print the crate version from a Cargo.toml file
set -o errexit -o pipefail
grep ^version Cargo.toml | sed 's%version = "\([^"]*\)"%\1%'
